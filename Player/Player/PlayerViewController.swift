//
//  PlayerViewController.swift
//  Player
//
//  Created by Nissi Vieira Miranda on 19/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import UIKit

class PlayerViewController: UIViewController {
    
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var playPannelA: PlayPannel!
    @IBOutlet weak var playPannelB: PlayPannel!
    @IBOutlet weak var playHeadA: PlayHead!
    @IBOutlet weak var playHeadB: PlayHead!
    @IBOutlet weak var playHeadMicroloopA: PlayHead!
    @IBOutlet weak var playHeadMicroloopB: PlayHead!
    @IBOutlet weak var ffButtonA: UIButton!
    @IBOutlet weak var ffButtonB: UIButton!
    @IBOutlet weak var microloopButton: UIButton!
    @IBOutlet weak var playButton: PlayButton!
    @IBOutlet weak var barLed: Led!
    @IBOutlet weak var beatLed: Led!
    @IBOutlet weak var musicalTimeIndicator: UILabel!
    
    @IBOutlet weak var playHeadALeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var playHeadBLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var playHeadMicroloopALeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var playHeadMicroloopBLeftConstraint: NSLayoutConstraint!
    
    private var playHeadPathLength: CGFloat = 0
    private var timeAlreadyPlayed: CGFloat = 0
    private var percentage: CGFloat = 0
    private var pathAlreadyWalked: CGFloat = 0
    private var timer: Timer?
    private var musicalTimeCalculator = MusicalTimeCalculator()
    
    // Microloop:
    private var minPosition: CGFloat = 0
    private var maxPosition: CGFloat = 0
    private var timeToPlay: CGFloat = 0
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        playPannelA.setItems(["A1", "A2", "A3"])
        playPannelB.setItems(["B1", "B2", "B3"])
        
        playPannelA.delegate = self
        playPannelB.delegate = self
        
        playButton.delegate = self
        musicalTimeCalculator.delegate = self
        
        Constants.NOTIF.addObserver(self,
        selector: #selector(playAudioNotification),
        name: Notification.Name(Constants.PLAY_AUDIO_NOTIF),
        object: nil)
        
        Constants.NOTIF.addObserver(self,
        selector: #selector(pauseAudioNotification),
        name: Notification.Name(Constants.PAUSE_AUDIO_NOTIF),
        object: nil)
        
        Constants.NOTIF.addObserver(self,
        selector: #selector(stopAudioNotification),
        name: Notification.Name(Constants.STOP_AUDIO_NOTIF),
        object: nil)
        
        Constants.NOTIF.addObserver(self,
        selector: #selector(audioEndedNotification),
        name: Notification.Name(Constants.AUDIO_ENDED_NOTIF),
        object: nil)
        
        Constants.NOTIF.addObserver(self,
        selector: #selector(playMicroloopAudioNotification),
        name: Notification.Name(Constants.PLAY_MICROLOOP_AUDIO_NOTIF),
        object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        playHeadPathLength = playPannelA.frame.width - 10.0
    }
    
    deinit {
        Constants.NOTIF.removeObserver(self)
    }
    
    private func resetPlayheads() {
        
        playHeadA.layer.removeAllAnimations()
        playHeadB.layer.removeAllAnimations()
        
        playHeadALeftConstraint.constant = 0
        playHeadBLeftConstraint.constant = 0
        view.layoutIfNeeded()
    }
    
    @IBAction func ffButtonATapped(_ sender: UIButton) {
        if playPannelA.increaseMax() {
            sender.isEnabled = false
        }
    }
    
    @IBAction func ffButtonBTapped(_ sender: UIButton) {
        if playPannelB.increaseMax() {
            sender.isEnabled = false
        }
    }
    
    @IBAction func microloopButtonTapped(_ sender: UIButton) {
        
        if microloopButton.isSelected {
            endMicroloop(sender)
        }
        else {
            startMicroloop()
        }
    }
}

extension PlayerViewController {
    
    private func startMicroloop() {
        
        guard (CGFloat(Constants.AUDIO_DURATION) - timeAlreadyPlayed) >= 3 else {
            return
        }
        
        playButton.setPlayingPaused()
        playButton.isInMicroloop = true
        
        microloopButton.isSelected = true
        
        let currentBars = musicalTimeCalculator.getCurrentBars()
        let secondsCurrentBars = TimeCalculator.barsToSeconds(currentBars)
        let percentageCurrentBars = (secondsCurrentBars * 0.01)/0.15
        
        let secondsTwoBars = TimeCalculator.barsToSeconds(2)
        let percentageTwoBars = (secondsTwoBars * 0.01)/0.15
        
        minPosition = playHeadPathLength * percentageCurrentBars
        timeToPlay = secondsTwoBars
        maxPosition = minPosition + (playHeadPathLength * percentageTwoBars)
        
        playHeadA.isHidden = true
        playHeadB.isHidden = true
        playHeadMicroloopA.isHidden = false
        playHeadMicroloopB.isHidden = false
        
        CentralClock.playMicroloopAudios(TimeInterval(timeToPlay))
    }
    
    private func endMicroloop(_ sender: UIButton?) {
        
        microloopButton.isSelected = false
        
        minPosition = 0
        timeToPlay = 0
        maxPosition = 0
        
        playHeadMicroloopA.layer.removeAllAnimations()
        playHeadMicroloopB.layer.removeAllAnimations()
        
        playHeadMicroloopALeftConstraint.constant = 0
        playHeadMicroloopBLeftConstraint.constant = 0
        view.layoutIfNeeded()
        
        playHeadA.isHidden = false
        playHeadB.isHidden = false
        playHeadMicroloopA.isHidden = true
        playHeadMicroloopB.isHidden = true
        
        CentralClock.stopMicroloopAudios()
        
        if sender != nil {
            playButton.isInMicroloop = false
            playButton.setPlayingPaused()
        }
    }
    
    @objc func playMicroloopAudioNotification() {
        
        playHeadMicroloopA.layer.removeAllAnimations()
        playHeadMicroloopB.layer.removeAllAnimations()
        
        playHeadMicroloopALeftConstraint.constant = minPosition
        playHeadMicroloopBLeftConstraint.constant = minPosition
        view.layoutIfNeeded()
        
        animateMicroloopPlayHeads()
    }
}

extension PlayerViewController: NewSongProtocol {
    
    func playingNewSong(_ pannelId: String) {
        if playPannelA.getPannelId() == pannelId {
            ffButtonA.isEnabled = true
        }
        else {
            ffButtonB.isEnabled = true
        }
    }
}

extension PlayerViewController {
    
    @objc func playAudioNotification() {
        
        animatePlayHeads()
        timer = Timer.scheduledTimer(timeInterval: 0.15, target: self, selector: #selector(playing), userInfo: nil, repeats: true)
    }
    
    @objc func pauseAudioNotification() {
        
        if let t = timer {
            t.invalidate()
        }
        
        playHeadA.layer.removeAllAnimations()
        playHeadB.layer.removeAllAnimations()
        
        playHeadALeftConstraint.constant = pathAlreadyWalked
        playHeadBLeftConstraint.constant = pathAlreadyWalked
        view.layoutIfNeeded()
    }
    
    @objc func stopAudioNotification() {
        
        if let t = timer {
            t.invalidate()
        }
        
        resetPlayheads()
        timeAlreadyPlayed = 0
        percentage = 0
        pathAlreadyWalked = 0
        musicalTimeCalculator.reset()
        musicalTimeIndicator.text = "0:0:0"
    }
    
    @objc func audioEndedNotification() {
        
        if let t = timer {
            t.invalidate()
        }
        
        resetPlayheads()
        timeAlreadyPlayed = 0
        percentage = 0
        pathAlreadyWalked = 0
        musicalTimeCalculator.playingNext()
        CentralClock.playAudios()
    }
}

extension PlayerViewController {
    
    @objc func playing() {
        timeAlreadyPlayed += 0.15
        percentage += 0.01
        pathAlreadyWalked = playHeadPathLength * percentage
        musicalTimeCalculator.buildCurrent(timeAlreadyPlayed)
    }
    
    @objc func turnOffBeat() {
        beatLed.turnOff()
    }
    
    @objc func turnOffBar() {
        barLed.turnOff()
    }
}

extension PlayerViewController: PlayButtonProtocol {
    
    func playAudio() {
        
        if microloopButton.isSelected {
            endMicroloop(nil)
        }
        else {
            CentralClock.playAudios()
        }
    }
    
    func pauseAudio() {
        CentralClock.pauseAudios()
    }
    
    func stopAudio() {
        CentralClock.stopAudios()
    }
}

extension PlayerViewController: MusicalTimeCalculatorProtocol {
    
    func beat() {
        beatLed.turnOn()
        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(turnOffBeat), userInfo: nil, repeats: false)
    }
    
    func bar() {
        barLed.turnOn()
        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(turnOffBar), userInfo: nil, repeats: false)
    }
    
    func currentMusicalTime(_ musicalTime: String) {
        musicalTimeIndicator.text = musicalTime
    }
}

extension PlayerViewController {
    
    private func animatePlayHeads() {
        
        let timeLeft = CGFloat(Constants.AUDIO_DURATION) - timeAlreadyPlayed
        
        UIView.animate(withDuration: TimeInterval(timeLeft),
                       delay: 0,
                       options: .curveLinear,
                       animations: {
                        
            self.playHeadALeftConstraint.constant = self.playHeadPathLength
            self.playHeadBLeftConstraint.constant = self.playHeadPathLength
            self.view.layoutIfNeeded()
                        
        }, completion: {
            (finished) -> Void in
        })
    }
    
    private func animateMicroloopPlayHeads() {
        
        let options: UIView.AnimationOptions = [.curveLinear, .repeat]
        
        UIView.animate(withDuration: TimeInterval(timeToPlay),
                       delay: 0,
                       options: options,
                       animations: {
                        
            self.playHeadMicroloopALeftConstraint.constant = self.maxPosition
            self.playHeadMicroloopBLeftConstraint.constant = self.maxPosition
            self.view.layoutIfNeeded()
                        
        }, completion: {
            (finished) -> Void in
        })
    }
}
