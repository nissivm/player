//
//  AudioFileType.swift
//  Player
//
//  Created by Nissi Vieira Miranda on 20/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation
import AVFoundation

class AudioFileType {
    static func getRawType(_ type: AVFileType) -> String {
        switch type.rawValue {
        case AVFileType.wav.rawValue:
            return "wav"
        case AVFileType.mp3.rawValue:
            return "mp3"
        default:
            print("(AudioFileType) Unsupported type")
        }
        return ""
    }
}
