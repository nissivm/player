//
//  AudioUrl.swift
//  Player
//
//  Created by Nissi Vieira Miranda on 21/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation
import AVFoundation

class AudioUrl {
    
    static func get(_ name: String, _ type: AVFileType) -> URL? {
        
        let rawType = AudioFileType.getRawType(type)
        
        guard rawType != "" else {
            return nil
        }
        
        guard let url = Bundle.main.url(forResource: name, withExtension: rawType) else {
            print("(AudioUrl) Could not find audio file")
            return nil
        }
        
        return url
    }
}
