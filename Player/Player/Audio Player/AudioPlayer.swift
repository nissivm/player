//
//  AudioPlayer.swift
//  Player
//
//  Created by Nissi Vieira Miranda on 20/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation
import AVFoundation

class AudioPlayer: NSObject {
    
    private var player: AVAudioPlayer?
    private var currentAudioName = ""
    private var currentTime: TimeInterval = 0
    
    private var minTime: TimeInterval = 0
    private var maxTime: TimeInterval = 0
    
    func play(_ audioName: String, _ type: AVFileType) -> Bool {
        
        guard let player = player else {
            return playFromStart(audioName, type)
        }
        
        if currentAudioName != "" {
            return resumePlaying(player)
        }
        else {
            return playFromStart(audioName, type)
        }
    }
    
    func pause() {
        
        guard let player = player else { return }
        guard player.isPlaying else { return }
        player.pause()
        
        currentTime = player.currentTime
        
        let minBars = TimeCalculator.secondsToBars(player.currentTime)
        minTime = TimeInterval(TimeCalculator.barsToSeconds(minBars))
        let secondsTwoBars = TimeCalculator.barsToSeconds(2)
        maxTime = minTime + TimeInterval(secondsTwoBars)
    }
    
    func stop() {
        
        guard let player = player else { return }
        player.stop()
        player.currentTime = 0
        self.player = nil
        currentAudioName = ""
    }
}

extension AudioPlayer {
    
    private func playFromStart(_ audioName: String, _ type: AVFileType) -> Bool {
        
        guard let url = AudioUrl.get(audioName, type) else {
            return false
        }
        
        do {
            
            // iOS 11+
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AudioFileType.getRawType(type))
            
            guard let player = player else { return false }
            
            currentAudioName = audioName
            
            player.play()
            
            return true
            
        } catch let error {
            print("(AudioPlayer) Error setting up player -> \(error.localizedDescription)")
            return false
        }
    }
    
    private func resumePlaying(_ player: AVAudioPlayer) -> Bool {
        
        if player.isPlaying {
            player.pause()
        }
        
        player.currentTime = currentTime
        player.play()
        
        return true
    }
}

extension AudioPlayer {
    
    func playMicroloopAudio() {
        
        guard let player = player else { return }
        
        if player.isPlaying {
            player.pause()
        }
        
        player.currentTime = minTime
        player.play()
    }
    
    func stopMicroloopAudio() {
        
        guard let player = player else { return }
        
        if player.isPlaying {
            player.pause()
        }
    }
}
