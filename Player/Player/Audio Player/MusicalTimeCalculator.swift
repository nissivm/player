//
//  MusicalTimeCalculator.swift
//  Player
//
//  Created by Nissi Vieira Miranda on 21/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation
import UIKit

class MusicalTimeCalculator {
    
    private var currentSixteenths: Int = 0
    private var totalSixteenths: Int = 0
    private var currentBars: Int = 0
    private var totalBars: Int = 0
    
    weak var delegate: MusicalTimeCalculatorProtocol?
    
    func buildCurrent(_ currentTime: CGFloat) {
        
        var currentMT = "0:0:0"
        currentSixteenths = Int(((125*4) * currentTime)/60)
        let loopSixteenths = totalSixteenths + currentSixteenths
        
        if currentSixteenths >= 16 {
            currentBars = currentSixteenths/16
        }
        
        if loopSixteenths <= 3 {
            
            currentMT = "0:0:3"
            
            if let del = delegate {
                del.currentMusicalTime(currentMT)
            }
            
            return
        }
        
        if loopSixteenths <= 15 {
            
            let beats = loopSixteenths/4
            let sixteenths = loopSixteenths%4
            
            if [1, 2, 3].contains(beats) {
                if let del = delegate {
                    del.beat()
                }
            }
            
            currentMT = "0:\(beats):\(sixteenths)"
            
            if let del = delegate {
                del.currentMusicalTime(currentMT)
            }
            
            return
        }
        
        let bars = loopSixteenths/16
        var beats: Int = 0
        var sixteenths: Int = 0
        
        let rest = loopSixteenths%16
        
        if rest > 0 {
            if rest <= 3 {
                sixteenths = rest
            }
            else {
                beats = rest/4
                sixteenths = rest%4
            }
        }
        
        if bars > totalBars {
            totalBars = bars
            if let del = delegate {
                del.bar()
            }
        }
        
        if [1, 2, 3].contains(beats) {
            if let del = delegate {
                del.beat()
            }
        }
        
        currentMT = "\(bars):\(beats):\(sixteenths)"
        
        if let del = delegate {
            del.currentMusicalTime(currentMT)
        }
    }
    
    func playingNext() {
        totalSixteenths += currentSixteenths
        currentSixteenths = 0
    }
    
    func reset() {
        currentSixteenths = 0
        totalSixteenths = 0
        currentBars = 0
        totalBars = 0
    }
    
    func getCurrentBars() -> Int {
        return currentBars
    }
}
