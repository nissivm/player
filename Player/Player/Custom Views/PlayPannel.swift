//
//  PlayPannel.swift
//  Player
//
//  Created by Nissi Vieira Miranda on 19/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import UIKit
import AVFoundation

class PlayPannel: UIImageView {
    
    private let audioPlayer = AudioPlayer()
    private var current = 0
    private var items = [String]()
    private var max = 0
    private var newSong = -1
    private var pannelId = ""
    
    weak var delegate: NewSongProtocol?
    
    private func initialize() {
        
        pannelId = UUID().uuidString
        
        Constants.NOTIF.addObserver(self,
        selector: #selector(playAudioNotification),
        name: Notification.Name(Constants.PLAY_AUDIO_NOTIF),
        object: nil)
        
        Constants.NOTIF.addObserver(self,
        selector: #selector(pauseAudioNotification),
        name: Notification.Name(Constants.PAUSE_AUDIO_NOTIF),
        object: nil)
        
        Constants.NOTIF.addObserver(self,
        selector: #selector(stopAudioNotification),
        name: Notification.Name(Constants.STOP_AUDIO_NOTIF),
        object: nil)
        
        Constants.NOTIF.addObserver(self,
        selector: #selector(audioEndedNotification),
        name: Notification.Name(Constants.AUDIO_ENDED_NOTIF),
        object: nil)
        
        Constants.NOTIF.addObserver(self,
        selector: #selector(playMicroloopAudioNotification),
        name: Notification.Name(Constants.PLAY_MICROLOOP_AUDIO_NOTIF),
        object: nil)
        
        Constants.NOTIF.addObserver(self,
        selector: #selector(stopMicroloopAudioNotification),
        name: Notification.Name(Constants.STOP_MICROLOOP_AUDIO_NOTIF),
        object: nil)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    deinit {
        Constants.NOTIF.removeObserver(self)
    }
}

extension PlayPannel {
    
    @objc func playAudioNotification() {
        
        guard items.count > 0 else { return }
        
        if audioPlayer.play(items[current], AVFileType.wav) {
            
            if current == newSong {
                if let del = delegate {
                    del.playingNewSong(pannelId)
                }
                newSong = -1
            }
            
            self.image = UIImage(named: items[current])
        }
        else {
            print("(PlayPannel) Error while trying play audio")
        }
    }
    
    @objc func pauseAudioNotification() {
        audioPlayer.pause()
    }
    
    @objc func stopAudioNotification() {
        audioPlayer.stop()
    }
    
    @objc func audioEndedNotification() {
        
        if current < max {
            current += 1
        }
        else {
            current = 0
        }
        
        audioPlayer.stop()
    }
}

extension PlayPannel {
    
    @objc func playMicroloopAudioNotification() {
        audioPlayer.playMicroloopAudio()
    }
    
    @objc func stopMicroloopAudioNotification() {
        audioPlayer.stopMicroloopAudio()
    }
}

extension PlayPannel {
    
    func setItems(_ newItems: [String]) {
        items = newItems
    }
    
    func increaseMax() -> Bool {
        if max + 1 <= (items.count - 1) {
            max += 1
            newSong = max
            return true
        }
        return false
    }
    
    func getPannelId() -> String {
        return pannelId
    }
}
