//
//  PlayHead.swift
//  Player
//
//  Created by Nissi Vieira Miranda on 19/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import UIKit

class PlayHead: UIView {

    override func draw(_ rect: CGRect) {
        self.layer.cornerRadius = 5.0
        self.layer.borderColor = UIColor(red: 100/255, green: 221/255, blue: 23/255, alpha: 1).cgColor
        self.layer.borderWidth = 1.0
        self.layer.backgroundColor = UIColor(red: 132/255, green: 1, blue: 1, alpha: 1).cgColor
        self.clipsToBounds = true
    }
}
