//
//  PlayButton.swift
//  Player
//
//  Created by Nissi Vieira Miranda on 21/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import UIKit

class PlayButton: UIImageView {
    
    weak var delegate: PlayButtonProtocol?
    private var isPlaying = false
    var isInMicroloop = false
    
    // INITIALIZATION
    
    private func initialize() {
        
        self.isUserInteractionEnabled = true
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(setStopped))
        let tap = UITapGestureRecognizer(target: self, action: #selector(setPlayingPaused))
        self.gestureRecognizers = [longPress, tap]
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    override func draw(_ rect: CGRect) {
        
        self.backgroundColor = UIColor.clear
        self.image = UIImage(named: "pause")
    }
    
    // GESTURES
    
    @objc func setPlayingPaused() {
        
        guard !isInMicroloop else {
            
            if let del = delegate {
                del.playAudio()
            }
            isInMicroloop = false
            return
        }
        
        if isPlaying {
            if let del = delegate {
                del.pauseAudio()
            }
        }
        else {
            if let del = delegate {
                del.playAudio()
            }
        }
        
        toggleState()
    }
    
    @objc func setStopped() {
        
        if !isPlaying {
            return
        }
        
        if let del = delegate {
            del.stopAudio()
        }
        
        toggleState()
    }
    
    // TOGGLE STATE
    
    func toggleState() {
        
        if isPlaying {
            isPlaying = false
            self.image = UIImage(named: "play")
        }
        else {
            isPlaying = true
            self.image = UIImage(named: "pause")
        }
    }
}
