//
//  Led.swift
//  Player
//
//  Created by Nissi Vieira Miranda on 19/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import UIKit

class Led: UIView {
    
    override func draw(_ rect: CGRect) {
        self.layer.cornerRadius = rect.size.width/2
        self.clipsToBounds = true
        turnOff()
    }

    func turnOn() {
        self.layer.borderColor = UIColor(red: 118/255, green: 1, blue: 3/255, alpha: 1).cgColor
        self.layer.borderWidth = 2
        self.layer.backgroundColor = UIColor(red: 118/255, green: 1, blue: 3/255, alpha: 1).cgColor
    }
    
    func turnOff() {
        self.layer.borderColor = UIColor.clear.cgColor
        self.layer.borderWidth = 0
        self.layer.backgroundColor = UIColor.gray.cgColor
    }
}
