//
//  TimeCalculator.swift
//  Player
//
//  Created by Nissi Vieira Miranda on 24/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation
import UIKit

class TimeCalculator {
    
    static func barsToSeconds(_ numBars: Int) -> CGFloat {
        
        let barsInOneMinute: CGFloat = 125/4
        let secondsPerBar = 60/barsInOneMinute
        return secondsPerBar * CGFloat(numBars)
    }
    
    static func secondsToBars(_ seconds: TimeInterval) -> Int {
        
        let barsInOneMinute: TimeInterval = 125/4
        let barsPerSecond = barsInOneMinute/60
        return Int(floor(barsPerSecond * seconds))
    }
}
