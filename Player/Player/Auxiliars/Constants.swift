//
//  Constants.swift
//  Player
//
//  Created by Nissi Vieira Miranda on 23/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

class Constants {
    
    static let AUDIO_DURATION: Int = 15
    static let PLAY_AUDIO_NOTIF = "PlayAudioNotification"
    static let PAUSE_AUDIO_NOTIF = "PauseAudioNotification"
    static let STOP_AUDIO_NOTIF = "StopAudioNotification"
    static let AUDIO_ENDED_NOTIF = "AudioEndedNotification"
    
    static let PLAY_MICROLOOP_AUDIO_NOTIF = "PlayMicroloopAudioNotification"
    static let STOP_MICROLOOP_AUDIO_NOTIF = "StopMicroloopAudioNotification"
    
    static let NOTIF = NotificationCenter.default
}
