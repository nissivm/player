//
//  MusicalTimeCalculatorProtocol.swift
//  Player
//
//  Created by Nissi Vieira Miranda on 21/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

protocol MusicalTimeCalculatorProtocol: class {
    
    func beat()
    func bar()
    func currentMusicalTime(_ musicalTime: String)
}
