//
//  PlayButtonProtocol.swift
//  Player
//
//  Created by Nissi Vieira Miranda on 21/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

protocol PlayButtonProtocol: class {
    func playAudio()
    func pauseAudio()
    func stopAudio()
}
