//
//  NewSongProtocol.swift
//  Player
//
//  Created by Nissi Vieira Miranda on 24/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

protocol NewSongProtocol: class {
    func playingNewSong(_ pannelId: String)
}
