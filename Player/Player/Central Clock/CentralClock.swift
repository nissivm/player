//
//  CentralClock.swift
//  Player
//
//  Created by Nissi Vieira Miranda on 23/04/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

class CentralClock {
    
    static private var counter: Int = 0
    static private var timer: Timer?
    
    static private var microloopTimer: Timer?
    
    static func playAudios() {
        
        Constants.NOTIF.post(name: Notification.Name(Constants.PLAY_AUDIO_NOTIF),
                object: nil)
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(playing), userInfo: nil, repeats: true)
    }
    
    static func pauseAudios() {
        Constants.NOTIF.post(name: Notification.Name(Constants.PAUSE_AUDIO_NOTIF),
                             object: nil)
        if let t = timer {
            t.invalidate()
        }
    }
    
    static func stopAudios() {
        Constants.NOTIF.post(name: Notification.Name(Constants.STOP_AUDIO_NOTIF),
                             object: nil)
        if let t = timer {
            t.invalidate()
            counter = 0
        }
    }
    
    @objc static func playing() {
        counter += 1
        if counter == Constants.AUDIO_DURATION {
            if let t = timer {
                t.invalidate()
                counter = 0
                Constants.NOTIF.post(name: Notification.Name(Constants.AUDIO_ENDED_NOTIF),
                                     object: nil)
            }
        }
    }
}

extension CentralClock {
    
    static func playMicroloopAudios(_ timeToPlay: TimeInterval) {
        
        let name = Notification.Name(Constants.PLAY_MICROLOOP_AUDIO_NOTIF)
        Constants.NOTIF.post(name: name, object: nil)
        
        microloopTimer = Timer.scheduledTimer(timeInterval: timeToPlay, target: self, selector: #selector(playingMicroloop), userInfo: nil, repeats: true)
    }
    
    static func stopMicroloopAudios() {
        
        if let timer = microloopTimer {
            timer.invalidate()
            microloopTimer = nil
        }
        
        let name = Notification.Name(Constants.STOP_MICROLOOP_AUDIO_NOTIF)
        Constants.NOTIF.post(name: name, object: nil)
    }
    
    @objc static func playingMicroloop() {
        let name = Notification.Name(Constants.PLAY_MICROLOOP_AUDIO_NOTIF)
        Constants.NOTIF.post(name: name, object: nil)
    }
}
